package com.train.company.data.input;

import com.train.company.core.STATION;

public class Tap {

    private int unixTimestamp;

    private Integer customerId;

    private STATION station;

    public Tap() {
    }

    public Tap(int unixTimestamp, Integer customerId, STATION station) {
        this.unixTimestamp = unixTimestamp;
        this.customerId = customerId;
        this.station = station;
    }


    public int getUnixTimestamp() {
        return unixTimestamp;
    }

    public void setUnixTimestamp(int unixTimestamp) {
        this.unixTimestamp = unixTimestamp;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public STATION getStation() {
        return station;
    }

    public void setStation(STATION station) {
        this.station = station;
    }

    @Override
    public String toString() {
        return "ClassPojo [unixTimestamp = " + unixTimestamp + ", customerId = " + customerId + ", station = " + station + "]";
    }
}
