package com.train.company.data.input;

import java.util.List;

public class BulkTaps {
    private List<Tap> taps;

    public List<Tap> getTaps() {
        return taps;
    }

    public void setTaps(List<Tap> taps) {
        this.taps = taps;
    }

    @Override
    public String toString() {
        return "BulkTaps{" +
                "taps=" + taps +
                '}';
    }
}
