package com.train.company.data;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

public class Serializer {
    private ObjectMapper objectMapper;

    public Serializer(){
        this.objectMapper = new ObjectMapper();
    }
    public void write(String path, Object object) throws IOException {
        objectMapper.writeValue(new File(path),object);
    }
}
