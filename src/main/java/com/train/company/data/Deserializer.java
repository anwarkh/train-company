package com.train.company.data;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

public class Deserializer {

    private ObjectMapper objectMapper;

    public Deserializer(){
        this.objectMapper = new ObjectMapper();
    }
    public <T> T read(String path, Class<T> clazz) throws IOException {
        return objectMapper.readValue(new File(path),clazz);
    }
}
