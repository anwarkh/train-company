package com.train.company.data;

import com.train.company.data.ouput.CustomerSummary;
import com.train.company.data.ouput.Trip;

import java.util.List;
import java.util.stream.Collectors;

public class Mapper {
    public List<Trip> convert(List<com.train.company.core.Trip> trips) {
        return trips.stream()
                .map(this::convert)
                .collect(Collectors.toList());
    }

    public Trip convert(com.train.company.core.Trip trip) {
        return Trip.Builder
                .create()
                .costInCents(trip.getCostInCents())
                .startedJourneyAt(trip.getStartedJourneyAt())
                .stationEnd(trip.getStationEnd())
                .stationStart(trip.getStationStart())
                .zoneFrom(trip.getZoneFrom().getName())
                .zoneTo(trip.getZoneTo().getName())
                .build();


    }

    public List<CustomerSummary> covert(List<com.train.company.core.CustomerSummary> customerSummariesList) {
        return customerSummariesList.stream()
                .map(this::convert)
                .collect(Collectors.toList());
    }

    private CustomerSummary convert(com.train.company.core.CustomerSummary customerSummary) {
        return new CustomerSummary(customerSummary.getCustomerId(),
                convert(customerSummary.getTrips()),
                customerSummary.getTotalCostInCents()
        );
    }
}
