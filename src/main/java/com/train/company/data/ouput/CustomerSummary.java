package com.train.company.data.ouput;


import java.util.List;
import java.util.Objects;

public class CustomerSummary {

    private Integer customerId;
    private int totalCostInCents;
    private List<Trip> trips;

    public CustomerSummary() {
    }

    public CustomerSummary(Integer customerId, List<Trip> trips, int totalCostInCents) {
        this.customerId = customerId;
        this.trips = trips;
        this.totalCostInCents = totalCostInCents;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public List<Trip> getTrips() {
        return trips;
    }

    public void setTrips(List<Trip> trips) {
        this.trips = trips;
    }

    public int getTotalCostInCents() {
        return totalCostInCents;
    }

    public void setTotalCostInCents(int totalCostInCents) {
        this.totalCostInCents = totalCostInCents;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomerSummary that = (CustomerSummary) o;
        return totalCostInCents == that.totalCostInCents &&
                Objects.equals(customerId, that.customerId) &&
                Objects.equals(trips, that.trips);
    }

    @Override
    public int hashCode() {

        return Objects.hash(customerId, totalCostInCents, trips);
    }
}
