package com.train.company.data.ouput;

import com.train.company.core.STATION;

import java.util.Objects;

public class Trip {
    private int costInCents;

    private STATION stationEnd;

    private Integer zoneTo;

    private int startedJourneyAt;

    private STATION stationStart;

    private Integer zoneFrom;

    public Trip() {
    }

    private Trip(int costInCents, STATION stationEnd, Integer zoneTo, int startedJourneyAt, STATION stationStart, Integer zoneFrom) {
        this.costInCents = costInCents;
        this.stationEnd = stationEnd;
        this.zoneTo = zoneTo;
        this.startedJourneyAt = startedJourneyAt;
        this.stationStart = stationStart;
        this.zoneFrom = zoneFrom;
    }

    public int getCostInCents() {
        return costInCents;
    }

    public void setCostInCents(int costInCents) {
        this.costInCents = costInCents;
    }

    public STATION getStationEnd() {
        return stationEnd;
    }

    public void setStationEnd(STATION stationEnd) {
        this.stationEnd = stationEnd;
    }

    public Integer getZoneTo() {
        return zoneTo;
    }

    public void setZoneTo(Integer zoneTo) {
        this.zoneTo = zoneTo;
    }

    public int getStartedJourneyAt() {
        return startedJourneyAt;
    }

    public void setStartedJourneyAt(int startedJourneyAt) {
        this.startedJourneyAt = startedJourneyAt;
    }

    public STATION getStationStart() {
        return stationStart;
    }

    public void setStationStart(STATION stationStart) {
        this.stationStart = stationStart;
    }

    public Integer getZoneFrom() {
        return zoneFrom;
    }

    public void setZoneFrom(Integer zoneFrom) {
        this.zoneFrom = zoneFrom;
    }

    public static class Builder {
        private int costInCents;
        private STATION stationEnd;
        private Integer zoneTo;
        private int startedJourneyAt;
        private STATION stationStart;
        private Integer zoneFrom;

        public static Builder  create(){
            return new Builder();
        }

        public Builder costInCents(int costInCents) {
            this.costInCents = costInCents;
            return this;
        }

        public Builder stationEnd(STATION stationEnd) {
            this.stationEnd = stationEnd;
            return this;
        }

        public Builder zoneTo(Integer zoneTo) {
            this.zoneTo = zoneTo;
            return this;
        }

        public Builder startedJourneyAt(int startedJourneyAt) {
            this.startedJourneyAt = startedJourneyAt;
            return this;
        }

        public Builder stationStart(STATION stationStart) {
            this.stationStart = stationStart;
            return this;
        }

        public Builder zoneFrom(Integer zoneFrom) {
            this.zoneFrom = zoneFrom;
            return this;
        }

        public Trip build() {
            return new Trip(costInCents, stationEnd, zoneTo, startedJourneyAt, stationStart, zoneFrom);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Trip trip = (Trip) o;
        return costInCents == trip.costInCents &&
                startedJourneyAt == trip.startedJourneyAt &&
                stationEnd == trip.stationEnd &&
                Objects.equals(zoneTo, trip.zoneTo) &&
                stationStart == trip.stationStart &&
                Objects.equals(zoneFrom, trip.zoneFrom);
    }

    @Override
    public int hashCode() {

        return Objects.hash(costInCents, stationEnd, zoneTo, startedJourneyAt, stationStart, zoneFrom);
    }
}
