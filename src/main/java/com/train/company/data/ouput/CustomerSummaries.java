package com.train.company.data.ouput;

import java.util.List;
import java.util.Objects;

public class CustomerSummaries {
    public CustomerSummaries() {
    }

    private List<CustomerSummary> customerSummaries;

    public CustomerSummaries(List<CustomerSummary> customerSummaries) {
        this.customerSummaries = customerSummaries;
    }

    public List<CustomerSummary> getCustomerSummaries() {
        return customerSummaries;
    }

    public void setCustomerSummaries(List<CustomerSummary> customerSummaries) {
        this.customerSummaries = customerSummaries;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomerSummaries that = (CustomerSummaries) o;
        return Objects.equals(customerSummaries, that.customerSummaries);
    }

    @Override
    public int hashCode() {

        return Objects.hash(customerSummaries);
    }
}
