package com.train.company.data.ouput;

import com.train.company.data.input.Tap;

import java.util.List;

public class CustomerJourney {
    private Integer customerId;
    private List<Tap> taps;

    public CustomerJourney() {
    }

    public CustomerJourney(Integer customerId, List<Tap> taps) {
        this.customerId = customerId;
        this.taps = taps;
    }

    public List<Tap> getTaps() {
        return taps;
    }

    public void setTaps(List<Tap> taps) {
        this.taps = taps;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    @Override
    public String toString() {
        return "ClassPojo [taps = " + taps + "]";
    }
}
