package com.train.company;

import com.train.company.core.AppManager;

public class App {
    public static void main(String[] args) {
        if(args != null && args.length >1){
            String inpuPath = args[0];
            String outputPath = args[1];
            if(inpuPath != null && outputPath != null){

                AppManager manager = new AppManager();
                manager.process(inpuPath,outputPath);
            }
        }


    }
}
