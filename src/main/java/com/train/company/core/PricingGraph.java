package com.train.company.core;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class PricingGraph {
    private final Set<Zone> zones; //collection of all zones

    public PricingGraph() {
        zones = new HashSet<>();
    }

    public List<Zone> getZones() {
        return new ArrayList<>(zones);
    }

    public boolean addZone(Zone zone) {
        return zones.add(zone);
    }

    public Set<Zone> findZoneByStation(STATION departure) {
       return zones.stream()
               .filter(zone -> zone.getStations().contains(departure))
               .collect(Collectors.toSet());
    }
}
