package com.train.company.core;


import java.util.List;

public class CustomerSummary {

    private final Integer customerId;
    private final int totalCostInCents;
    private final List<Trip> trips;

    public CustomerSummary(CustomerJourney journey) {
        this.customerId = journey.getCustomerId();
        this.trips = journey.generateTrips();
        this.totalCostInCents = calculateTotalCost();
    }

    private int calculateTotalCost() {
        return trips.stream()
                .mapToInt(Trip::getCostInCents)
                .sum();
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public int getTotalCostInCents() {
        return totalCostInCents;
    }

    public List<Trip> getTrips() {
        return trips;
    }


}


