package com.train.company.core;


public class Connection {
    private final Zone arrival;
    private final int price;

    public Connection(Zone arrival, int price) {

        this.arrival = arrival;
        this.price = price;
    }


    public Zone getArrival() {
        return arrival;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Connection{" +
                "arrival=" + arrival +
                ", price=" + price +
                '}';
    }
}
