package com.train.company.core;

import com.train.company.data.input.Tap;
import javafx.util.Pair;

public class Trip {

    private STATION stationStart;
    private STATION stationEnd;

    private int startedJourneyAt;
    private int costInCents;

    private Zone zoneFrom;
    private Zone zoneTo;


    private Trip(int costInCents, STATION stationEnd, Zone zoneTo, int startedJourneyAt, STATION stationStart, Zone zoneFrom) {
        this.costInCents = costInCents;
        this.stationEnd = stationEnd;
        this.zoneTo = zoneTo;
        this.startedJourneyAt = startedJourneyAt;
        this.stationStart = stationStart;
        this.zoneFrom = zoneFrom;
    }

    public int getCostInCents() {
        return costInCents;
    }

    public STATION getStationEnd() {
        return stationEnd;
    }

    public Zone getZoneTo() {
        return zoneTo;
    }

    public int getStartedJourneyAt() {
        return startedJourneyAt;
    }

    public STATION getStationStart() {
        return stationStart;
    }

    public Zone getZoneFrom() {
        return zoneFrom;
    }


    public static class Builder {
        private int costInCents;
        private STATION stationEnd;
        private Zone zoneTo;
        private int startedJourneyAt;
        private STATION stationStart;
        private Zone zoneFrom;

        public static Builder create() {
            return new Builder();
        }

        public Builder costInCents(int costInCents) {
            this.costInCents = costInCents;
            return this;
        }

        public Builder stationEnd(STATION stationEnd) {
            this.stationEnd = stationEnd;
            return this;
        }

        public Builder zoneTo(Zone zoneTo) {
            this.zoneTo = zoneTo;
            return this;
        }

        public Builder startedJourneyAt(int startedJourneyAt) {
            this.startedJourneyAt = startedJourneyAt;
            return this;
        }

        public Builder stationStart(STATION stationStart) {
            this.stationStart = stationStart;
            return this;
        }

        public Builder zoneFrom(Zone zoneFrom) {
            this.zoneFrom = zoneFrom;
            return this;
        }

        public Builder tripCost(TripCostCalculator.TripCost tripCost) {
            this.zoneFrom = tripCost.getFrom();
            this.zoneTo = tripCost.getTo();
            this.costInCents = tripCost.getCost();
            return this;
        }

        public Trip build(Pair<Tap, Tap> tapPair) {
            TripCostCalculator tripCostCalculator = new TripCostCalculator();

            startedJourneyAt(tapPair.getKey().getUnixTimestamp())
                    .stationStart(tapPair.getKey().getStation())
                    .stationEnd(tapPair.getValue().getStation())
                    .tripCost(tripCostCalculator.calculate(
                            tapPair.getKey().getStation(),
                            tapPair.getValue().getStation())
                    );


            return new Trip(costInCents, stationEnd, zoneTo, startedJourneyAt, stationStart, zoneFrom);
        }
    }
}
