package com.train.company.core;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

import static com.train.company.core.STATION.*;

public class TripCostCalculator {

    private final PricingGraph pricingGraph;

    public TripCostCalculator() {
        this.pricingGraph = new PricingGraph();
        initStationsGraph();
    }

    TripCost calculate(STATION departure, STATION arrival) {
        Set<Zone> candidateDepartureZones = pricingGraph.findZoneByStation(departure);
        Set<Zone> candidateArrivalZones = pricingGraph.findZoneByStation(arrival);


        return candidateDepartureZones.stream()
                .flatMap(x -> candidateArrivalZones.stream()
                        .map(y -> new TripCost(x, y, x.tripCostTo(y))))
                .min(Comparator.comparing(t -> t.getCost()))
                .orElse(null);


    }


    protected void initStationsGraph() {

        Zone zone1 = new Zone(1, new HashSet<>(Arrays.asList(A, B)));
        Zone zone2 = new Zone(2, new HashSet<>(Arrays.asList(C, D, E)));
        Zone zone3 = new Zone(3, new HashSet<>(Arrays.asList(C, E, F)));
        Zone zone4 = new Zone(4, new HashSet<>(Arrays.asList(F, G, H, I)));


//        1) Travel within zones 1 and 2: €2.40 per trip.
        zone1.addConnection(new Connection(zone2, 240));
        zone2.addConnection(new Connection(zone1, 240));

//        2) Travel within zones 3 and 4: €2.00 per trip.
        zone3.addConnection(new Connection(zone4, 200));
        zone4.addConnection(new Connection(zone3, 200));


//        3) Travel from zone 3 to zone 1 or 2: €2.80 per trip.
        zone3.addConnection(new Connection(zone1, 280));
        zone3.addConnection(new Connection(zone2, 280));

//        4) Travel from zone 4 to Zone 1 or 2: €3.00 per trip.
        zone4.addConnection(new Connection(zone1, 300));
        zone4.addConnection(new Connection(zone2, 300));

//        5) Travel from zone 1 or 2, to zone 3: €2.80 per trip.
        zone1.addConnection(new Connection(zone3, 280));
        zone2.addConnection(new Connection(zone3, 280));

//        6) Travel from zone 1 or 2, zone 4 to: €3.00 per trip.
        zone1.addConnection(new Connection(zone4, 300));
        zone2.addConnection(new Connection(zone4, 300));

        pricingGraph.addZone(zone1);
        pricingGraph.addZone(zone2);
        pricingGraph.addZone(zone3);
        pricingGraph.addZone(zone4);
    }

    public class TripCost {
        private Zone from;
        private Zone to;
        private int cost;

        public TripCost(Zone from, Zone to, int cost) {
            this.from = from;
            this.to = to;
            this.cost = cost;
        }

        public Zone getFrom() {
            return from;
        }

        public void setFrom(Zone from) {
            this.from = from;
        }

        public Zone getTo() {
            return to;
        }

        public void setTo(Zone to) {
            this.to = to;
        }

        public int getCost() {
            return cost;
        }

        public void setCost(int cost) {
            this.cost = cost;
        }
    }
}
