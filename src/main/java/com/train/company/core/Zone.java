package com.train.company.core;

import javafx.util.Pair;

import java.util.Comparator;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

public class Zone {

    private final Integer name;
    private final Set<Connection> connections;
    private final Set<STATION> stations;

    public Zone(Integer name, Set<STATION> stations) {
        this.name = name;
        this.stations = stations;
        connections = new TreeSet<>(Comparator.comparing(o -> o.getPrice(), (o1, o2) -> o1 < o2 ? -1 : 1));
    }

    public Integer getName() {
        return name;
    }

    public Set<Connection> getConnections() {
        return connections;
    }

    public Set<STATION> getStations() {
        return stations;
    }

    public Set<Connection> addConnection(Connection connection) {
        connections.add(connection);
        return connections;
    }

    public int tripCostTo(Zone zone) {
        return connections.stream()
                .filter(connection -> connection.getArrival().equals(zone))
                .findFirst()
                .map(connection -> connection.getPrice())
                .orElse(0);
    }

    public Pair<Zone, Integer> cheapestTripTo(Set<Zone> zones) {
        return zones.stream()
                .map(zone -> new Pair(zone, tripCostTo(zone)))
                .min(Comparator.comparing(o -> (Integer) o.getValue()))
                .orElse(null);


    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Zone zone = (Zone) o;
        return Objects.equals(name, zone.name) &&
                Objects.equals(stations, zone.stations);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, stations);
    }
}
