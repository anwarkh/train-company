package com.train.company.core;

import com.train.company.data.Deserializer;
import com.train.company.data.Mapper;
import com.train.company.data.Serializer;
import com.train.company.data.input.BulkTaps;
import com.train.company.data.input.Tap;
import com.train.company.data.ouput.CustomerSummaries;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class AppManager {
    private Mapper mapper;


    public AppManager() {
        this.mapper = new Mapper();
    }

    public void process(String inputFile, String outputFile) {

        BulkTaps taps = loadTaps(inputFile);
        CustomerSummaries summaries = createSummaries(taps);
        saveSummaries(outputFile, summaries);
    }

    private CustomerSummaries createSummaries(BulkTaps bulkTaps) {
        List<CustomerSummary> customerSummariesList = bulkTaps.getTaps()
                        .stream()
                        .collect(Collectors.groupingBy(Tap::getCustomerId))
                        .entrySet().stream()
                        .map(e -> new CustomerJourney(e.getKey(), e.getValue()))
                        .map(CustomerSummary::new)
                        .collect(Collectors.toList());

        return new CustomerSummaries(mapper.covert(customerSummariesList));
    }


    private void saveSummaries(String output, CustomerSummaries customerSummaries) {
        Serializer serializer = new Serializer();
        try {
            serializer.write(output, customerSummaries);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private BulkTaps loadTaps(String input) {
        Deserializer deserializer = new Deserializer();
        BulkTaps bulkTaps = null;
        try {
            bulkTaps = deserializer.read(input, BulkTaps.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bulkTaps;
    }


}
