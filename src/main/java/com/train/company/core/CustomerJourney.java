package com.train.company.core;

import com.train.company.data.input.Tap;
import javafx.util.Pair;

import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class CustomerJourney {
    private final Integer customerId;
    private final List<Tap> taps;


    public CustomerJourney(Integer customerId, List<Tap> taps) {
        this.customerId = customerId;
        this.taps = taps;
        this.taps.sort(Comparator.comparing(Tap::getUnixTimestamp));
    }

    public List<Tap> getTaps() {
        return taps;
    }


    public Integer getCustomerId() {
        return customerId;
    }


    public List<Trip> generateTrips() {
        return gatherTaps().stream()
                .map(tapPair -> Trip.Builder
                        .create()
                        .build(tapPair))
                .collect(Collectors.toList());
    }


    protected List<Pair<Tap, Tap>> gatherTaps() {
        final AtomicInteger counter = new AtomicInteger(0);
        return taps.stream()
                .collect(Collectors.groupingBy(s -> counter.getAndIncrement() / 2))
                .values()
                .stream()
                .map(taps1 -> new Pair<Tap, Tap>(taps1.get(0), taps1.get(1)))
                .collect(Collectors.toList());

    }
}
