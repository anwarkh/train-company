package com.train.company.core;

import com.train.company.data.input.Tap;
import javafx.util.Pair;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class CustomerJourneyTest {

    @Test
    public void testGatherTaps() {
        //GIVEN
        AppManager appManager = new AppManager();
        Integer customerId = 1;
        List<Tap> taps = Arrays.asList(new Tap(1572242400, customerId, STATION.A),
                new Tap(1572242401, customerId, STATION.D),
                new Tap(1572242402, customerId, STATION.C),
                new Tap(1572242403, customerId, STATION.E)

        );

        CustomerJourney journey = new CustomerJourney(customerId, taps);


        //WHEN
        List<Pair<Tap, Tap>> pairs = journey.gatherTaps();
        //THEN
        Assert.assertEquals(2, pairs.size());
        Assert.assertEquals(STATION.A,pairs.get(0).getKey().getStation());
        Assert.assertEquals(STATION.D,pairs.get(0).getValue().getStation());
        Assert.assertEquals(STATION.C,pairs.get(1).getKey().getStation());
        Assert.assertEquals(STATION.E,pairs.get(1).getValue().getStation());
    }

    @Test
    public void generateTrips() {
        //GIVEN
        AppManager appManager = new AppManager();
        Integer customerId = 1;
        List<Tap> taps = Arrays.asList(new Tap(1572242400, customerId, STATION.A),
                new Tap(1572242401, customerId, STATION.D),
                new Tap(1572242402, customerId, STATION.C),
                new Tap(1572242403, customerId, STATION.E)

        );

        CustomerJourney journey = new CustomerJourney(customerId, taps);


        //WHEN
        List<Trip> trips = journey.generateTrips();
        //THEN
        Assert.assertEquals(2, trips.size());
        Assert.assertEquals(STATION.A,trips.get(0).getStationStart());
        Assert.assertEquals(STATION.D,trips.get(0).getStationEnd());

        Assert.assertEquals(STATION.C,trips.get(1).getStationStart());
        Assert.assertEquals(STATION.E,trips.get(1).getStationEnd());

    }

    @Test
    public void generateTrips_with_unordered_taps() {
        //GIVEN
        AppManager appManager = new AppManager();
        Integer customerId = 1;
        List<Tap> taps = Arrays.asList(
                new Tap(1572242401, customerId, STATION.D),
                new Tap(1572242403, customerId, STATION.E),
                new Tap(1572242402, customerId, STATION.C),
                new Tap(1572242400, customerId, STATION.A)

        );

        CustomerJourney journey = new CustomerJourney(customerId, taps);


        //WHEN
        List<Trip> trips = journey.generateTrips();
        //THEN
        Assert.assertEquals(2, trips.size());
        Assert.assertEquals(STATION.A,trips.get(0).getStationStart());
        Assert.assertEquals(STATION.D,trips.get(0).getStationEnd());

        Assert.assertEquals(STATION.C,trips.get(1).getStationStart());
        Assert.assertEquals(STATION.E,trips.get(1).getStationEnd());

    }

}