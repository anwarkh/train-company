package com.train.company.core;

import com.train.company.data.input.Tap;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class CustomerSummaryTest {

    @Test
    public void testCreateCustomerSummary_with_two_taps() {
        //GIVEN
        AppManager appManager = new AppManager();
        Integer customerId = 1;
        List<Tap> taps = Arrays.asList(new Tap(1572242400, customerId, STATION.A),
                new Tap(1572242401, customerId, STATION.D)

        );

        CustomerJourney journey = new CustomerJourney(customerId, taps);



        CustomerSummary customerSummary = new CustomerSummary(journey);
        //THEN
        Assert.assertEquals(new Integer(1), customerSummary.getCustomerId());
        Assert.assertEquals(1, customerSummary.getTrips().size());


    }

}