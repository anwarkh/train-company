package com.train.company.core;

import com.train.company.data.Deserializer;
import com.train.company.data.ouput.CustomerSummaries;
import com.train.company.data.ouput.CustomerSummary;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class AppManagerIntegrationTest {

    private Deserializer deserializer;

    @Before
    public void setup() {
        deserializer = new Deserializer();
    }


    @Test
    public void testProcess_basic_test() throws IOException {
        AppManager appManager = new AppManager();
        String outputFile = "src/test/resources/basic-test/output_file.json";
        String referenceOutputFile = "src/test/resources/basic-test/reference_output_file.json";
        String inputFile = "src/test/resources/basic-test/input_file.json";
        appManager.process(inputFile, outputFile);

        com.train.company.data.ouput.CustomerSummaries referenceResult = deserializer.read(referenceOutputFile, com.train.company.data.ouput.CustomerSummaries.class);
        com.train.company.data.ouput.CustomerSummaries customerSummaries = deserializer.read(outputFile, com.train.company.data.ouput.CustomerSummaries.class);

        Assert.assertEquals(referenceResult, customerSummaries);

    }

    @Test
    public void testProcess_400_item_test() throws IOException {
        AppManager appManager = new AppManager();
        String outputFile = "src/test/resources/400-item-test/output_file.json";
        String inputFile = "src/test/resources/400-item-test/input_file.json";
        appManager.process(inputFile, outputFile);

        com.train.company.data.ouput.CustomerSummaries customerSummaries = deserializer.read(outputFile, com.train.company.data.ouput.CustomerSummaries.class);

        Assert.assertEquals(4, customerSummaries.getCustomerSummaries().size());

        CustomerSummary customer1Summary = getSummaryByCustomerId(customerSummaries, 1);
        CustomerSummary customer2Summary = getSummaryByCustomerId(customerSummaries, 2);
        CustomerSummary customer3Summary = getSummaryByCustomerId(customerSummaries, 3);
        CustomerSummary customer4Summary = getSummaryByCustomerId(customerSummaries, 4);

        Assert.assertNotNull(customer1Summary);
        Assert.assertEquals(50, customer1Summary.getTrips().size());
        Assert.assertNotNull(customer2Summary);
        Assert.assertEquals(50, customer2Summary.getTrips().size());
        Assert.assertNotNull(customer3Summary);
        Assert.assertEquals(50, customer3Summary.getTrips().size());
        Assert.assertNotNull(customer4Summary);
        Assert.assertEquals(50, customer4Summary.getTrips().size());

    }

    private CustomerSummary getSummaryByCustomerId(CustomerSummaries customerSummaries, int i) {
        return customerSummaries.getCustomerSummaries().stream().filter(customerSummary -> customerSummary.getCustomerId() == i).findFirst().orElse(null);
    }
}