package com.train.company.core;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;

import static com.train.company.core.STATION.*;

public class ZoneTest {

    @Test
    public void findCheapestCost() {

        Zone zone1 = new Zone(1, new HashSet<>(Arrays.asList(A, B)));
        Zone zone2 = new Zone(2, new HashSet<>(Arrays.asList(C, D, E)));
        Zone zone3 = new Zone(3, new HashSet<>(Arrays.asList(C, E, F)));
        Zone zone4 = new Zone(4, new HashSet<>(Arrays.asList(F, G, H, I)));


        zone1.addConnection(new Connection(zone1, 1));
        zone1.addConnection(new Connection(zone2, 20));
        zone1.addConnection(new Connection(zone2, 60));
        zone1.addConnection(new Connection(zone2, 1));
        zone1.addConnection(new Connection(zone2, 5));
        zone1.addConnection(new Connection(zone3, 3));
        zone1.addConnection(new Connection(zone4, 4));
        zone1.addConnection(new Connection(zone2, 3));
        zone1.addConnection(new Connection(zone2, 4));
        Assert.assertEquals(1, zone1.tripCostTo(zone2));

    }
}