package com.train.company.data;

import com.train.company.data.ouput.CustomerJourney;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class DeserializerTest {

    private Deserializer deserializer;

    @Before
    public void setup() {
        deserializer = new Deserializer();
    }

    @Test
    public void read() throws IOException {
        String inptFile = getClass().getClassLoader().getResource("input_file.json").getFile();
        CustomerJourney journey = deserializer.read(inptFile, CustomerJourney.class);
        Assert.assertNotNull(journey);
        System.out.println(journey);
    }
}