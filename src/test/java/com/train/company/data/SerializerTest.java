package com.train.company.data;

import com.train.company.core.STATION;
import com.train.company.data.ouput.CustomerSummaries;
import com.train.company.data.ouput.CustomerSummary;
import com.train.company.data.ouput.Trip;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SerializerTest {

    private Serializer serializer;
    private Deserializer deserializer;

    @Before
    public void setup() {
        serializer = new Serializer();
    }

    @Test
    public void read() throws IOException {
        CustomerSummary customerSummary = new CustomerSummary(1, Arrays.asList(Trip.Builder
                .create()
                .stationStart(STATION.A)
                .stationEnd(STATION.E)
                .build()),
                280);
        List<CustomerSummary> customerSummaryList = new ArrayList<>();
        customerSummaryList.add(customerSummary);

        CustomerSummaries customerSummaries = new CustomerSummaries(customerSummaryList);

        String outputFile = getClass().getClassLoader().getResource("output_file.json").getFile();
        serializer.write(outputFile, customerSummaries);

    }
}
